const fs = require('fs');
const csv = require('csv-parser');

//filepath
MATCHES_FILE_PATH = 'src/data/matches.csv';
// Function to read the CSV file and export the data to another module

const matchesPerYear = {};

fs.createReadStream(MATCHES_FILE_PATH)
  .pipe(csv())
  .on('data', (row) => {
    // console.log(row)
    const year = row.season;
    if (matchesPerYear[year]) {
      matchesPerYear[year] += 1;
    } else {
      matchesPerYear[year] = 1;
    }

  })
  .on('end', () => {
    const jsonData = JSON.stringify(matchesPerYear, null, 2);

    // Write the JSON data to a file
    fs.writeFile('src/public/output/matchesPerYear.json', jsonData, 'utf8', (err) => {
      if (err) {
        console.error('Error writing JSON file:', err);
        return;
      }
      console.log('Data written to JSON file successfully.');
    });
    //    console.log(matchesPerYear)
  });








