const fs = require('fs');
const csv = require('csv-parser');

const MATCHES_FILE_PATH = 'src/data/matches.csv';
const DELIVERY_FILE_PATH = 'src/data/deliveries.csv';

const strikeRate = {};
const batsman_run = {};
const batsman_ball = {};
matchSeason = {};
years = ['2017', '2008', '2009', '2010', '2011', '2012', '2013', '2014', '2015', '2016']


// Read matches.csv file
fs.createReadStream(MATCHES_FILE_PATH)
    .pipe(csv())
    .on('data', (matchesRow) => {
        const id = matchesRow.id;
        const year = matchesRow.season;
        matchSeason[id] = year;
    })
    .on('end', () => {

        fs.createReadStream(DELIVERY_FILE_PATH)
            .pipe(csv())
            .on('data', (deliveryRow) => {
                const id = deliveryRow.match_id;
                const run = parseInt(deliveryRow.batsman_runs);
                const batsman = deliveryRow.batsman;
                const year = matchSeason[id];
                if (!batsman_ball[year]) {
                    batsman_ball[year] = {};
                    batsman_run[year] = {};
                }
                if (!batsman_ball[year][batsman]) {
                    batsman_ball[year][batsman] = run;
                    batsman_run[year][batsman] = 1
                }
                else {
                    batsman_ball[year][batsman] += run;
                    batsman_run[year][batsman] += 1;
                }
            })


            .on('end', () => {
                for (id in batsman_ball) {
                    const value_batsman = batsman_ball[id];

                    for (key in value_batsman) {
                        if (!strikeRate[id]) {
                            strikeRate[id] = {};
                        }

                        strikeRate[id][key] = (value_batsman[key] / batsman_run[id][key]) * 100;


                    }
                }


                const jsonData = JSON.stringify(strikeRate, null, 2);

                // Write the JSON data to a file
                fs.writeFile('src/public/output/strikeRatePerSeason.json', jsonData, 'utf8', (err) => {
                    if (err) {
                        console.error('Error writing JSON file:', err);
                        return;
                    }
                    console.log('Data written to JSON file successfully.');
                });

                console.log(strikeRate);
            });
    });

