const fs = require('fs');
const csv = require('csv-parser');

//filepath
MATCHES_FILE_PATH = 'src/data/matches.csv';
// Function to read the CSV file and export the data to another module

const matchesWonPerYear = {};

fs.createReadStream(MATCHES_FILE_PATH)
    .pipe(csv())
    .on('data', (row) => {
        const year = row.season;
        const team = row.winner;
        if (!matchesWonPerYear[year]) {
            matchesWonPerYear[year] = {};
        }
        if (matchesWonPerYear[year][team]) {
            matchesWonPerYear[year][team] += 1;
        }
        else {
            matchesWonPerYear[year][team] = 1;
        }

    })
    .on('end', () => {
        const jsonData = JSON.stringify(matchesWonPerYear, null, 2);

        // Write the JSON data to a file
        fs.writeFile('src/public/output/matchesWonPerTeamPerYear.json', jsonData, 'utf8', (err) => {
            if (err) {
                console.error('Error writing JSON file:', err);
                return;
            }
            console.log('Data written to JSON file successfully.');
        });
        console.log(matchesWonPerYear)
    });








