const fs = require('fs');
const csv = require('csv-parser');

const MATCHES_FILE_PATH = 'src/data/matches.csv';
const DELIVERY_FILE_PATH = 'src/data/deliveries.csv';

const super_over_bowler = {};
const bowler_run = {};
const bowler_ball = {};

// Read matches.csv file
fs.createReadStream(DELIVERY_FILE_PATH)
    .pipe(csv())
    .on('data', (deliveryRow) => {
        const bowler = deliveryRow.bowler;
        const run = parseInt(deliveryRow.total_runs);
        if (deliveryRow.is_super_over != 0) {
            //    console.log(bowler, run);
            if (bowler_run[bowler]) {
                bowler_run[bowler] += run;
                bowler_ball[bowler] += 1;
            }
            else {
                bowler_run[bowler] = run;
                bowler_ball[bowler] = 1;
            }
        }

    })

    .on('end', () => {
        let topEconomicalBowler = {}
        for (key in bowler_run) {
            topEconomicalBowler[key] = (bowler_run[key] / (bowler_ball[key] / 6));
        }
        const entries = Object.entries(topEconomicalBowler);

        // Sort the array based on the values in descending order
        entries.sort((a, b) => a[1] - b[1]);

        // Get only the top 10 elements
        const topBowler = entries.slice(0, 1);

        // Convert the top 10 array back into an object
        const top1Bowler = Object.fromEntries(topBowler);


        const jsonData = JSON.stringify(top1Bowler, null, 2);

        // Write the JSON data to a file
        fs.writeFile('src/public/output/topEconomicalBowlerSuperOver.json', jsonData, 'utf8', (err) => {
            if (err) {
                console.error('Error writing JSON file:', err);
                return;
            }
            console.log('Data written to JSON file successfully.');
        });

        // console.log(bowler_ball);
    });

