const fs = require('fs');
const csv = require('csv-parser');

const MATCHES_FILE_PATH = 'src/data/matches.csv';
const playerOfMatch = {};
const top5PlayerOfMatch={};

// Read matches.csv file
fs.createReadStream(MATCHES_FILE_PATH)
    .pipe(csv())
    .on('data', (matchRow) => {
        const player = matchRow.player_of_match;
        const year = matchRow.season;
        if(!playerOfMatch[year]){
            playerOfMatch[year]={};
        }
        if (playerOfMatch[year][player]) {
            playerOfMatch[year][player] += 1;
        }
        else {
            playerOfMatch[year][player] = 1;
        }
    })
    .on('end', () => {
        for(key in playerOfMatch){

        const entries = Object.entries(playerOfMatch[key]);

        // Sort the array based on the values in descending order
        entries.sort((a, b) => b[1] - a[1]);

        // Get only the top 5 elements
        const topPlayer = entries.slice(0, 5);

        // Convert the top 5 array back into an object
        const topPlayerOfMatch = Object.fromEntries(topPlayer);
        top5PlayerOfMatch[key] = topPlayerOfMatch;

    }


        const jsonData = JSON.stringify(top5PlayerOfMatch, null, 2);

        // Write the JSON data to a file
        fs.writeFile('src/public/output/topPlayerOfMatchPerSeason.json', jsonData, 'utf8', (err) => {
            if (err) {
                console.error('Error writing JSON file:', err);
                return;
            }
            console.log('Data written to JSON file successfully.');
        });

        // console.log(top5PlayerOfMatch);
    });

